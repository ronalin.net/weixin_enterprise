- Install Steps
	- apt-get install pip
	- pip install tornado
	- apt-get install python-dev
	- pip install pycrypto
#微信企业号消息发送接口封装


微信企业号提供了七大类的消息类型，除了mpnews(会存在微信自己的服务器上，感觉没有必要)外都已封装。

## 接口文件
1. webchartapi.py 消息类型及发送方法
2. hello.py 		最早基于tornado写的企业服务器，即回调URL的服务，配置应用的回调URL时需要提供验证的方法，可参考其中的代码

	**注意：其他代码无用**


##消息类说明

### Message 
消息类型的基本类，不应用于实际的消息创建，为所有消息类型提供如下公共参数：

	touser : 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。
			  特殊情况：指定为@all，则向关注该企业应用的全部成员发送.非必填.
    
    agentid: 企业应用的id，整型。可在应用的设置页面查看.
    
    toparty: 部门ID列表，多个接收者用‘|’分隔，最多支持100个。
    		  当touser为@all时忽略本参数
    
    totag  : 标签ID列表，多个接收者用‘|’分隔。当touser为@all时忽略本参数
    
    safe   : 表示是否是保密消息，0表示否，1表示是，默认0

### BinaryMessage
附件类消息的基类，继承于Message。比父类多提供两个参数 type和media_id。不应直接创建对象使用。


### TextMessage
文本类型消息，继承于Message,比父类多一个content参数。

**创建方式**：
	
<code>
m = TextMessage(agentid,touser='@all',toparty='',totag='',safe=0,content='我就是要发送的内容')	
</code>


### FileMessage
普通文件消息，用来向微信发送文件下载的消息。继承于BinaryMessage。
<code>
m = FileMessage(agentid,touser='@all',toparty='',totag='',safe=0,media_id='bdko23sdskjd')	
</code>

### ImageMessage
图片消息，与FileMessage相同

### InvoiceMessage
声音消息，与FileMessage相同

### VideoMessage
视频类型消息。
<code>
m = VideoMessage(agentid,touser='@all',toparty='',totag='',safe=0,media_id='bdko23sdskjd',title="这是我的一段自拍视频",description="赶紧看看...")
</code>

###NewsMessage 
News消息,此消息稍微复杂，和Article对象配合使用，继承于Message类，需要额外提供news参数。
news : 消息列表，结构如下[Article(),Article(),...]

创建方式如下 :

<code>
news = [
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
    ]
    message = NewsMessage(3,touser="@all",news=news)
</code>


## WebChartApi 消息发送类
此类提供消息发送、文件上传、获取token、验证回调URL有效性四个主要方法。

###初始化对象:

<code>
	api = WebChartApi(sCorpID,corpsecret)
</code>

###验证回调URL
需要在自己的WEB服务器上调用此方法来验证

	def verify(self,stoken,msg_signature,timestamp,echostr,nonce):
        """
        验证企业号的回调URL的有效性
        Args:
            stoken:       应用上设置的用于回调的token.
            msg_signature:微信加密签名，msg_signature结合了企业填写的token、
            				 请求中的timestamp、nonce参数、加密的消息体.
            timestamp:    时间戳.
            echostr:      加密的随机字符串，以msg_encrypt格式提供。需要解密并返回echostr明文，
                          解密后有random、msg_len、msg、$CorpID四个字段，其中msg即为echostr
                          明文.
            nonce:        随机数.
        Returns:
            ret:          为0时表验证成功，其他返回值请参考微信官方文档.
            sEchoStr:     解密之后的echostr，当ret返回0时有效
        """
        echostr = urllib.unquote(echostr)
        wxcpt = WXBizMsgCrypt(stoken,self.corpsecret,sCorpID)
        ret,sEchoStr = wxcpt.VerifyURL(msg_signature[0],timestamp,nonce,echostr)
        return ret,sEchoStr
### 获取token
<code>
	api = WebChartApi(sCorpID,corpsecret)<br/>
	 token = api.get_access_token()
</code>

### 上传文件

<code>
	api = WebChartApi(sCorpID,corpsecret) <br/>
	token = api.upload_media("文件类型","文件地址")
</code>

### 发送消息

<code>
	api = WebChartApi(sCorpID,corpsecret) <br/>
	token = api.send_message(m1)
</code>

## 代码示例

**上传附件**

	api = WebChartApi(sCorpID,corpsecret)
    media_id = api.upload_media("image","/Users/XXX/abc.docx")

**发送图片:**

	api = WebChartApi(sCorpID,corpsecret)
    media_id = api.upload_media("image","/Users/tedi/Downloads/logo.jpg")
    message = ImageMessage(3,touser="@all",media_id=media_id)
    res = api.send_message(message)
 
 
**发送文件**
 
	api = WebChartApi(sCorpID,corpsecret)
    media_id = api.upload_media("file","/Users/XXX/abc.docx")
    message = FileMessage(3,touser="@all",media_id=media_id)
    res = api.send_message(message)
    
    
**发送新闻**

	news = [
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
        Article("完全符合我的要求","description1","http://www.sian.com","http://img1.gtimg.com/ninja/0/ninja143114034284198.jpg"),
    ]
    message = NewsMessage(3,touser="@all",news=news)
    api = WebChartApi(sCorpID,corpsecret)
    res = api.send_message(message)
